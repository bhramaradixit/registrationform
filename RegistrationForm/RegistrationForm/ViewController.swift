//
//  ViewController.swift
//  RegistrationForm
//
//  Created by Bhramara Dixit on 02/06/17.
//  Copyright (c) 2017 Bhramara Dixit. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var firstNameField: UITextField!
    
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var genderControlBtn: UISegmentedControl!
    
    @IBOutlet weak var phoneNumberField: UITextField!
    
    @IBOutlet weak var emailAddressField: UITextField!
    
    @IBOutlet weak var jobTypeField: UITextField!
    
    @IBOutlet weak var eduField: UITextField!

    @IBOutlet weak var imageView: UIImageView!

    var jobPicker = UIPickerView()
    var eduPicker = UIPickerView()
    
    var jobTypes:[String]  = []
    var educationValues:[String]  = []

    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.populatePickerViews()

        jobPicker.dataSource = self
        jobPicker.delegate = self
        
        eduPicker.dataSource = self
        eduPicker.delegate = self
        
        jobTypeField.inputView = jobPicker
        eduField.inputView = eduPicker
        
    }

    @IBAction func submitForm(sender: AnyObject) {
        var url: NSURL = NSURL(string: "https://evaluation-exercise.herokuapp.com/profile")!
        var request:NSMutableURLRequest = NSMutableURLRequest(URL:url)
        request.HTTPMethod = "POST"

        var name = firstNameField.text
        var genderVal = genderControlBtn.titleForSegmentAtIndex(genderControlBtn.selectedSegmentIndex)
        var phoneNum = phoneNumberField.text
        var emailId = emailAddressField.text
        var jobType = jobTypeField.text
        var eduType = eduField.text
        
        var param = ["name": "\(name)", "gender":"\(genderVal)", "phoneNumber":"\(phoneNum)", "emailId":"\(emailId)", "jobType":"\(jobType)", "photoBitmap":"", "address":"", "latitude":"",  "longitude":"", "educationalQualification":"\(eduType)"]
        
        var error:NSError? = nil
        var jsonData = NSJSONSerialization.dataWithJSONObject(param,options:nil,error:&error)
        
        request.HTTPBody = jsonData;

            NSURLConnection.sendAsynchronousRequest(request,queue: NSOperationQueue.mainQueue()){
                    (response, data, error) in
                    print(NSString(data: data, encoding: NSUTF8StringEncoding))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func populatePickerViews(){
        let url = NSURL(string: "https://evaluation-exercise.herokuapp.com/getMasterData")!
        let data = NSData(contentsOfURL: url)
        var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
        //print("RESULT: \(jsonResult) ")
        
        var jobTypesarr = jsonResult.valueForKey("MasterData")?.valueForKey("JobTypes") as! NSArray
        
        for var j = 0; j<jobTypesarr.count; ++j {
            
            var jobTypeDict = jobTypesarr.objectAtIndex(j) as! NSDictionary
            let jobTypeVal = jobTypeDict.valueForKey("JobType") as! String
            jobTypes.append(jobTypeVal)
        }
        print("JOBTYPES: \(jobTypes) ")
        
        var eduValuesarr = jsonResult.valueForKey("MasterData")?.valueForKey("EducationalQualification") as! NSArray
        
        for var k = 0; k<eduValuesarr.count; ++k {
            
            var eduValueDict = eduValuesarr.objectAtIndex(k) as! NSDictionary
            let eduVal = eduValueDict.valueForKey("Qualification") as! String
            educationValues.append(eduVal)
        }
        print("educationValues: \(educationValues) ")

    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if (pickerView == jobPicker){
            return jobTypes.count
        }
        else{
            return educationValues.count
        
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if (pickerView == jobPicker){
            jobTypeField.text = jobTypes[row]
            jobPicker.resignFirstResponder()

        }
        else{
            eduField.text = educationValues[row]
            eduPicker.resignFirstResponder()
        }
        
        self.view.endEditing(false)

    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView == jobPicker){
            return jobTypes[row]
        }
        else{
            return educationValues[row]
        }

    }
}

